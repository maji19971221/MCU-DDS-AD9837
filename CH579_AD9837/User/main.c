#include "CH57x_common.h"
#include "config.h"
#include "HAL.h"
#include "gattprofile.h"
#include "peripheral.h"

#include "adc.h"
#include "myspi.h"
#include "ad9837.h"

u32 MEM_BUF[BLE_MEMHEAP_SIZE / 4];
#if (defined (BLE_MAC)) && (BLE_MAC == TRUE)//一个MAC地址，但不是芯片内部的MAC地址
u8C MacAddr[6] = {0x84,0xC2,0xE4,0x03,0x81,0x06};
#endif


UINT8 flag=0;
UINT16 Wave_Choose=0x2000;
extern UINT32 Fecy_number;
/* 100K1 */
int main(void)
{
#ifdef DEBUG
  GPIOA_SetBits(bTXD1);
  GPIOA_ModeCfg(bTXD1, GPIO_ModeOut_PP_5mA);
  UART1_DefInit( );
#endif 
	
	My_SPI_Init();
	CH57X_BLEInit( );
	HAL_Init( );
	GAPRole_PeripheralInit( );
	Peripheral_Init( ); 
	ADC_Tempture_init();//内置温度传感器采样初始化
	
	while(1)
	{
		TMOS_SystemProcess( );
		if(flag==0)
		{
			DelayUs(30);
			AD9837_Freq_Count();
			WriteToAD9837_Freq(Wave_Choose);
			Fecy_number=0;
			flag=1;
		}
	}
}
