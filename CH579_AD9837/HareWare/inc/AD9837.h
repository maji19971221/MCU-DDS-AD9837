#ifndef __AD9837_H__
#define __AD9837_H__

#include "CH57x_common.h"
#include "stdio.h"
#include "config.h"
#include "myspi.h"


void AD9837_Freq_Count(void);
void WriteToAD9837_Freq(UINT16 Wave_Choose);


#endif
