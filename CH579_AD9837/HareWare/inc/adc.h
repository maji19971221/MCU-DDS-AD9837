
#ifndef __ADC_H__
#define __ADC_H__

#include "CH57x_common.h"
#include "stdio.h"
#include "config.h"

typedef enum
{
	ADC_AIN0=0,
	ADC_AIN1,
	ADC_AIN2,
	ADC_AIN3,
	ADC_AIN4,
	ADC_AIN5,
	ADC_AIN6,
	ADC_AIN7,
	ADC_AIN8,
	ADC_AIN9,
	ADC_AIN10,
	ADC_AIN11,
	ADC_AIN12,
	ADC_AIN13,
}ADC_GPIO_ChoiceDef;



void ADC_Tempture_init(void);//采集内部温度传感器AD初始化
UINT16 Git_ADC_Tempture(void);//获取温度传感器温度



#endif
