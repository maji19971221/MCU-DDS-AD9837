#ifndef __MYSPI_H__
#define __MYSPI_H__

#include "CH57x_common.h"
#include "stdio.h"
#include "config.h"




#define Soft_SPI_CS_Low()				GPIOA_ResetBits(GPIO_Pin_13);  
#define Soft_SPI_CS_HIGH()			GPIOA_SetBits(GPIO_Pin_13);   

#define Soft_SPI_SCLK_LOW()				GPIOA_ResetBits(GPIO_Pin_14);  
#define Soft_SPI_SCLK_HIGH()			GPIOA_SetBits(GPIO_Pin_14); 

#define Soft_SPI_MOSI_LOW()				GPIOA_ResetBits(GPIO_Pin_15);  
#define Soft_SPI_MOSI_HIGH()			GPIOA_SetBits(GPIO_Pin_15); 

void My_SPI_Init(void);
void Soft_SPI_Write(uint16_t a);

#endif
