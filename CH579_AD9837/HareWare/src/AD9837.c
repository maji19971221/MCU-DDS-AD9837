

#include "ad9837.h"
#include "math.h"
#include "myspi.h"

UINT16 AD_LSB=0;//写入LSB的数据
UINT16 AD_MSB=0;//写入MSB的数据
UINT32 Fecy=0;//需要设置的频率
UINT32 AD9837_Freq;//频率计算后的数据

/* 计算频率对应的数据 */
void AD9837_Freq_Count(void)
{
	PRINT("AD9837_Fecy:%d\r\n",Fecy);
	AD9837_Freq = (Fecy*pow(2,28))/(16*pow(10,6));
	PRINT("AD9837_Freq:%d\r\n",AD9837_Freq);	
	PRINT("AD9837_Freq:%x",AD9837_Freq);	
	AD_LSB=(AD9837_Freq&0x3fff)+0x4000;
	AD_MSB=((AD9837_Freq>>14)&0x3fff)+0x4000;
	PRINT("AD_LSB:%x",AD_LSB);
	PRINT("AD_MSB:%x",AD_MSB);
}

/* AD9837配置 */
void WriteToAD9837_Freq(UINT16 Wave_Choose)
{ 
	Soft_SPI_CS_Low();//拉低片选线
	DelayUs(10);
	Soft_SPI_Write(0X2100);//D13=1,D8=1,reset
	Soft_SPI_CS_HIGH();
	
	DelayUs(30);
	
	Soft_SPI_CS_Low();//拉低片选线
	DelayUs(10);
	Soft_SPI_Write(AD_LSB);//D13=1,D8=1,reset 300Hz  控制频率7264  4110
	Soft_SPI_CS_HIGH();
	
	DelayUs(30);
	
	Soft_SPI_CS_Low();//拉低片选线
	DelayUs(10); 
	Soft_SPI_Write(AD_MSB);//D13=1,D8=1,reset  4fff  5481
	Soft_SPI_CS_HIGH();
	
	DelayUs(30);
		
	Soft_SPI_CS_Low();//拉低片选线
	DelayUs(10);
	Soft_SPI_Write(0XC000);//D13=1,D8=1,reset
	Soft_SPI_CS_HIGH();

	
	DelayUs(30);
	
	Soft_SPI_CS_Low();//拉低片选线
	DelayUs(10);
	Soft_SPI_Write(Wave_Choose);//D13=1,D8=1,reset  控制波形   2000-sin  2002-三角   2028-方波
	Soft_SPI_CS_HIGH();
	DelayUs(30);
}

