#include "adc.h"


signed short  RoughCalib_Value=0;
/*温度*/
UINT16 TemptureAD_Value=0;
UINT16 TemptureValue=0;




/*******************************************************************************
* Function Name  : Git_ADC_Tempture
* Description    : 内部温度AD采集初始化，读取内部偏差
* Input          : 无
* Return         : 无
*******************************************************************************/
void ADC_Tempture_init(void)
{
	ADC_InterTSSampInit();
//	GPIOA_ModeCfg(GPIO_Pin_5, GPIO_ModeIN_Floating);
	RoughCalib_Value = ADC_DataCalib_Rough();//用于计算ADC内部偏差。记录到变量RoughCalib_Value中，注意这个变量需要定义为有符号变量
}


/*******************************************************************************
* Function Name  : Git_ADC_Tempture
* Description    : 获取温度传感器温度
* Input          : 无
* Return         : TemptureValue，返回采集到的温度℃
之前需要使用ADC_Tempture_init();//内置温度传感器采样初始化
*******************************************************************************/
UINT16 Git_ADC_Tempture(void)
{
	TemptureAD_Value = ADC_ExcutSingleConver() + RoughCalib_Value;
	TemptureValue=ADC_GetCurrentTS(TemptureAD_Value);//获取当前温度，内部AD值转换为℃
	return TemptureValue;
}

