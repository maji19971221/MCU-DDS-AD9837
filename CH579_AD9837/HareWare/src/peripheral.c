/********************************** (C) COPYRIGHT *******************************
* File Name          : peripheral.C
* Author             : WCH
* Version            : V1.0
* Date               : 2018/12/10
* Description        : 外设从机多连接应用程序，初始化广播连接参数，然后广播，连接主机后，
*                      请求更新连接参数，通过自定义服务传输数据           
*******************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "CONFIG.h"
#include "CH57x_common.h"
#include "devinfoservice.h"
#include "gattprofile.h"
#include "peripheral.h"
#include "adc.h"
#include "ad9837.h"
#include "math.h"

extern UINT32 Fecy;
UINT32 Fecy_number=0;

extern UINT16 Wave_Choose;
UINT16 Wave_Low,Wave_Hign;
uint8 newValue_len;
extern UINT8 flag;
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */
extern gattCharCfg_t simpleProfileChar4Config[4];
u8 a[]={9,8,7,6};
uint8 value1[5]={0x88,0x68,0x00,0x00,0x00};
UINT16 ADC_Tempture;

// How often to perform periodic event
#define SBP_PERIODIC_EVT_PERIOD               1600

// How often to perform read rssi event
#define SBP_READ_RSSI_EVT_PERIOD              3200

// Parameter update delay 
#define SBP_PARAM_UPDATE_DELAY                6400

// What is the advertising interval when device is discoverable (units of 625us, 80=50ms)
#define DEFAULT_ADVERTISING_INTERVAL          80

// Limited discoverable mode advertises for 30.72s, and then stops
// General discoverable mode advertises indefinitely
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL

// Minimum connection interval (units of 1.25ms, 20=25ms) 
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     20

// Maximum connection interval (units of 1.25ms, 100=125ms)
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     100

// Slave latency to use parameter update
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms, 100=1s)
#define DEFAULT_DESIRED_CONN_TIMEOUT          100

// Company Identifier: WCH  
#define WCH_COMPANY_ID                        0x07D7

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static uint8 Peripheral_TaskID = INVALID_TASK_ID;   // Task ID for internal task/event processing

// GAP - SCAN RSP data (max size = 31 bytes)
static uint8 scanRspData[ ] =
{
  // complete name
  0x07,   // length of this data
  GAP_ADTYPE_LOCAL_NAME_COMPLETE,
  'A',   
  'D',   
  '9',   
  '8', 
	'3',
	'7',
//  'a',   
//  'i',   
//  ' ',
//  'P',   
//  'e',   
//  'r',   
//  'i',   
//  'p',   
//  'h',   
//  'e',   
//  'r',   
//  'a',   
//  'l',   
  // connection interval range
  0x05,   // length of this data
  GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
  LO_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),   // 100ms
  HI_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),
  LO_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),   // 1s
  HI_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),

  // Tx power level
  0x02,   // length of this data
  GAP_ADTYPE_POWER_LEVEL,
  0       // 0dBm
};

// GAP - Advertisement data (max size = 31 bytes, though this is
// best kept short to conserve power while advertisting)
static uint8 advertData[] =
{
  // Flags; this sets the device to use limited discoverable
  // mode (advertises for 30 seconds at a time) instead of general
  // discoverable mode (advertises indefinitely)
  0x02,   // length of this data
  GAP_ADTYPE_FLAGS,
  DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

  // service UUID, to notify central devices what services are included
  // in this peripheral
  0x03,   // length of this data
  GAP_ADTYPE_16BIT_MORE,      // some of the UUID's, but not all
  LO_UINT16( SIMPLEPROFILE_SERV_UUID ),
  HI_UINT16( SIMPLEPROFILE_SERV_UUID )
};

// GAP GATT Attributes
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "Xiao Ma conn";//链接上去的名字

// Connection item list
static peripheralConnItem_t peripheralConnList;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void Peripheral_ProcessTMOSMsg( tmos_event_hdr_t *pMsg );
static void peripheralStateNotificationCB( gapRole_States_t newState ,gapRoleEvent_t * pEvent);
static void performPeriodicTask( void );
static void simpleProfileChangeCB( uint8 paramID );
static void peripheralParamUpdateCB( uint16 connHandle, uint16 connInterval, 
                                      uint16 connSlaveLatency, uint16 connTimeout );
static void peripheralInitConnItem( peripheralConnItem_t* peripheralConnList );
static void peripheralRssiCB( uint16 connHandle, int8  rssi );
static void peripheralChar4Notify( uint8 *pValue, uint16 len );

/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t Peripheral_PeripheralCBs =
{
  peripheralStateNotificationCB,  // Profile State Change Callbacks
  peripheralRssiCB,                   // When a valid RSSI is read from controller (not used by application)
	peripheralParamUpdateCB
};

// Broadcast Callbacks
static gapRolesBroadcasterCBs_t Broadcaster_BroadcasterCBs =
{
  NULL,  										// Not used in peripheral role
  NULL											// Receive scan request callback
};

// GAP Bond Manager Callbacks
static gapBondCBs_t Peripheral_BondMgrCBs =
{
  NULL,                     // Passcode callback (not used by application)
  NULL                      // Pairing / Bonding state Callback (not used by application)
};

// Simple GATT Profile Callbacks
static simpleProfileCBs_t Peripheral_SimpleProfileCBs =
{
  simpleProfileChangeCB    // Charactersitic value change callback
};
/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      Peripheral_Init
 *
 * @brief   Initialization function for the Peripheral App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by TMOS.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void Peripheral_Init( )
{
  Peripheral_TaskID = TMOS_ProcessEventRegister( Peripheral_ProcessEvent );
  
  // Setup the GAP Peripheral Role Profile  
  {
    uint8 initial_advertising_enable = TRUE;
    uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
    uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
    
    // Set the GAP Role Parameters  设置GAP Role参数     这边都是广播的数据   当广播ScanRspData是就是发送里面的数据就可以看到XiaoMa
    GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );//打开广播使能
    GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanRspData ), scanRspData );//当主机扫描到广播后，从机返回扫描回应数据
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertData ), advertData );//广播数据
    GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );//连接后的hop，一段时间进行交互最小连接间隔25ms
    GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );//最大间隔125ms  connect
  }

  // Set the GAP Characteristics  设置GAP    GATT服务参数
  GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );

  // Set advertising interval设置广播间隔
  {
    uint16 advInt = DEFAULT_ADVERTISING_INTERVAL;

    GAP_SetParamValue( TGAP_DISC_ADV_INT_MIN, advInt );//50ms
    GAP_SetParamValue( TGAP_DISC_ADV_INT_MAX, advInt );//50ms
  }

  // Setup the GAP Bond Manager  设置GAP Bond
  {
    uint32 passkey = 1234; // passkey "000001"
    uint8 pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;//是否允许配对：1.不允许 2.等待中央设备请求在配对 3.自己初始化配对     
		//配对模式，配置成等待主机的配置请求
    uint8 mitm = TRUE;//安全管理是否需要人为参与
    uint8 bonding = TRUE;//使能绑定
    uint8 ioCap = GAPBOND_IO_CAP_DISPLAY_ONLY;
    GAPBondMgr_SetParameter( GAPBOND_PERI_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
    GAPBondMgr_SetParameter( GAPBOND_PERI_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
    GAPBondMgr_SetParameter( GAPBOND_PERI_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
    GAPBondMgr_SetParameter( GAPBOND_PERI_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
    GAPBondMgr_SetParameter( GAPBOND_PERI_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
  }

  // Initialize GATT attributes  初始化GATT属性
  GGS_AddService( GATT_ALL_SERVICES );            // GAP
  GATTServApp_AddService( GATT_ALL_SERVICES );    // GATT attributes
  DevInfo_AddService();                           // Device Information Service
  SimpleProfile_AddService( GATT_ALL_SERVICES );  // Simple GATT Profile

  // Setup the SimpleProfile Characteristic Values 对相关数值进行初始化
  {
    uint8 charValue1[SIMPLEPROFILE_CHAR1_LEN] = { 0x00,0x20,0x00};
//    uint8 charValue2[SIMPLEPROFILE_CHAR2_LEN] = { 0x12 };
//    uint8 charValue3[SIMPLEPROFILE_CHAR3_LEN] = { 3 };//APP显示都是16进制数
    uint8 charValue4[SIMPLEPROFILE_CHAR4_LEN] = { 4 };
    uint8 charValue5[SIMPLEPROFILE_CHAR5_LEN] = { 0x12, 0x23, 0x34, 0x45, 0x56 };
		
    SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR1, SIMPLEPROFILE_CHAR1_LEN, charValue1 );
//    SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR2, SIMPLEPROFILE_CHAR2_LEN, charValue2 );
//    SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR3, SIMPLEPROFILE_CHAR3_LEN, charValue3 );
    SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR4, SIMPLEPROFILE_CHAR4_LEN, charValue4 );
    SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR5, SIMPLEPROFILE_CHAR5_LEN, charValue5 );
  }
  
  // Init Connection Item   连接初始化
  peripheralInitConnItem( &peripheralConnList );

  // Register callback with SimpleGATTprofile    给应用注册回调函数
  SimpleProfile_RegisterAppCBs( &Peripheral_SimpleProfileCBs );
 
  // Register receive scan request callback
	GAPRole_BroadcasterSetCB( &Broadcaster_BroadcasterCBs );//注册扫描回复回调函数
	
	// Setup a delayed profile startup
  tmos_set_event( Peripheral_TaskID, SBP_START_DEVICE_EVT );
}

/*********************************************************************
 * @fn      peripheralInitConnItem
 *
 * @brief   Init Connection Item
 *
 * @param   peripheralConnList -
 *
 * @return  NULL
 */
static void peripheralInitConnItem( peripheralConnItem_t* peripheralConnList )
{
  peripheralConnList->connHandle = GAP_CONNHANDLE_INIT;
  peripheralConnList->connInterval = 0;
  peripheralConnList->connSlaveLatency = 0;
  peripheralConnList->connTimeout = 0;
}

/*********************************************************************
 * @fn      Peripheral_ProcessEvent
 *
 * @brief   Peripheral Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id - The TMOS assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 Peripheral_ProcessEvent( uint8 task_id, uint16 events )
{

//  VOID task_id; // TMOS required parameter that isn't used in this function

  if ( events & SYS_EVENT_MSG )
	{
    uint8 *pMsg;
		printf("SYS_EVENT_MSG");
    if ( (pMsg = tmos_msg_receive( Peripheral_TaskID )) != NULL ){
      Peripheral_ProcessTMOSMsg( (tmos_event_hdr_t *)pMsg );
      // Release the TMOS message
      tmos_msg_deallocate( pMsg );
    }
    // return unprocessed events
    return (events ^ SYS_EVENT_MSG);
  }

  if ( events & SBP_START_DEVICE_EVT )
	{
	// Start the Device
		printf("Start the Device\r\n");
    GAPRole_PeripheralStartDevice( Peripheral_TaskID, &Peripheral_BondMgrCBs, &Peripheral_PeripheralCBs );//启动设备，注册回调函数，用于监督设备状态变化
    return ( events ^ SBP_START_DEVICE_EVT );
  }

  if ( events & SBP_PERIODIC_EVT )
  {
   // Restart timer
    if ( SBP_PERIODIC_EVT_PERIOD ){
      tmos_start_task( Peripheral_TaskID, SBP_PERIODIC_EVT, SBP_PERIODIC_EVT_PERIOD );
    }
    // Perform periodic application task 执行定期应用任务
//    performPeriodicTask();
//		SBP_SerialAppSendNoti(a,sizeof(a));
		value1_send_temture();
		//SendDate();
    return (events ^ SBP_PERIODIC_EVT);
  }
  
  if ( events & SBP_PARAM_UPDATE_EVT )//如果是更新事件
  {
    // Send connect param update request 发送链接参数更新请求
		printf("SBP_PARAM_UPDATE_EVT\r\n");
    GAPRole_PeripheralConnParamUpdateReq( peripheralConnList.connHandle,
                                          DEFAULT_DESIRED_MIN_CONN_INTERVAL,
                                          DEFAULT_DESIRED_MAX_CONN_INTERVAL,
                                          DEFAULT_DESIRED_SLAVE_LATENCY,
                                          DEFAULT_DESIRED_CONN_TIMEOUT,
                                          Peripheral_TaskID);
    
    return (events ^ SBP_PARAM_UPDATE_EVT);
  }
  
  if ( events & SBP_READ_RSSI_EVT )
  {
		printf("SBP_READ_RSSI_EVT\r\n");
    GAPRole_ReadRssiCmd(peripheralConnList.connHandle);//执行发送RSSI的代码
//    tmos_start_task( Peripheral_TaskID, SBP_READ_RSSI_EVT, SBP_READ_RSSI_EVT_PERIOD );      
    return (events ^ SBP_READ_RSSI_EVT);
  }   

  // Discard unknown events
  return 0;
}

/*********************************************************************
 * @fn      Peripheral_ProcessTMOSMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void Peripheral_ProcessTMOSMsg( tmos_event_hdr_t *pMsg )
{
  switch ( pMsg->event ){
		default:
			break;
  }
}

/*********************************************************************
 * @fn      Peripheral_LinkEstablished
 *
 * @brief   Process link established.
 *
 * @param   pEvent - event to process
 *
 * @return  none
 */
static void Peripheral_LinkEstablished( gapRoleEvent_t * pEvent ) //链接时
{
  gapEstLinkReqEvent_t *event = (gapEstLinkReqEvent_t *) pEvent;
  
  // See if already connected
  if( peripheralConnList.connHandle != GAP_CONNHANDLE_INIT )
  {
    GAPRole_TerminateLink( pEvent->linkCmpl.connectionHandle );
    PRINT( "Connection max...\n" );
  }
  else
  {
    peripheralConnList.connHandle = event->connectionHandle;
    peripheralConnList.connInterval = event->connInterval;
    peripheralConnList.connSlaveLatency = event->connLatency;
    peripheralConnList.connTimeout = event->connTimeout;
/*  主机不需要 notify同意 */    
//		simpleProfileChar4Config[0].connHandle =event->connectionHandle;//每一位代表一个连接
//		simpleProfileChar4Config[0].value=0x0001;
    // Set timer for periodic event
    tmos_start_task( Peripheral_TaskID, SBP_PERIODIC_EVT, SBP_PERIODIC_EVT_PERIOD );

    // Set timer for param update event
    tmos_start_task( Peripheral_TaskID, SBP_PARAM_UPDATE_EVT, SBP_PARAM_UPDATE_DELAY );
    
    // Start read rssi
    tmos_start_task( Peripheral_TaskID, SBP_READ_RSSI_EVT, SBP_READ_RSSI_EVT_PERIOD );
    
    PRINT("Conn %x - Int %x \n", event->connectionHandle, event->connInterval);                   
  }        
}

/*********************************************************************
 * @fn      Peripheral_LinkTerminated
 *
 * @brief   Process link terminated.
 *
 * @param   pEvent - event to process
 *
 * @return  none
 */
static void Peripheral_LinkTerminated( gapRoleEvent_t * pEvent )
{
  gapTerminateLinkEvent_t *event = (gapTerminateLinkEvent_t *) pEvent;
  
  if( event->connectionHandle == peripheralConnList.connHandle )
  {
    peripheralConnList.connHandle = GAP_CONNHANDLE_INIT;
    peripheralConnList.connInterval = 0;
    peripheralConnList.connSlaveLatency = 0;
    peripheralConnList.connTimeout = 0;
    tmos_stop_task( Peripheral_TaskID, SBP_PERIODIC_EVT );
    tmos_stop_task( Peripheral_TaskID, SBP_READ_RSSI_EVT );
    
    // Restart advertising
    {
      uint8 advertising_enable = TRUE;
      GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advertising_enable );
    }
  }
  else
  {
    PRINT("ERR..\n");
  }      
}

/*********************************************************************
 * @fn      peripheralRssiCB
 *
 * @brief   RSSI callback.
 *
 * @param   connHandle - connection handle
 * @param   rssi - RSSI
 *
 * @return  none
 */
static void peripheralRssiCB( uint16 connHandle, int8 rssi )
{
  PRINT( "RSSI -%d dB Conn  %x \n", -rssi, connHandle);
}

/*********************************************************************
 * @fn      peripheralParamUpdateCB
 *
 * @brief   Parameter update complete callback
 *
 * @param   connHandle - connect handle
 *          connInterval - connect interval
 *          connSlaveLatency - connect slave latency
 *          connTimeout - connect timeout
 *          
 * @return  none
 */
static void peripheralParamUpdateCB( uint16 connHandle, uint16 connInterval, 
                                      uint16 connSlaveLatency, uint16 connTimeout )
{
  if( connHandle == peripheralConnList.connHandle )
  {
    peripheralConnList.connInterval = connInterval;
    peripheralConnList.connSlaveLatency = connSlaveLatency;
    peripheralConnList.connTimeout = connTimeout;

    PRINT("Update %x - Int %x \n", connHandle, connInterval);
  }
  else
  {
    PRINT("ERR..\n");
  }
}

/*********************************************************************
 * @fn      peripheralStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void peripheralStateNotificationCB( gapRole_States_t newState, gapRoleEvent_t * pEvent )//传入参数由GAP自己输入，内部调用回调函数给用户
{
  switch ( newState )
  {
    case GAPROLE_STARTED:
      PRINT( "Initialized..\n" );
      break;

    case GAPROLE_ADVERTISING:
      if( pEvent->gap.opcode == GAP_LINK_TERMINATED_EVENT )//如果断开链接了
      {
        Peripheral_LinkTerminated( pEvent );
        PRINT( "Disconnected.. Reason:%x\n",pEvent->linkTerminate.reason );
      }
      PRINT( "Advertising..\n" );
      break;

    case GAPROLE_CONNECTED:
      if( pEvent->gap.opcode == GAP_LINK_ESTABLISHED_EVENT )
      {
        Peripheral_LinkEstablished( pEvent );
      }
      PRINT( "Connected..\n" );
      break;

    case GAPROLE_CONNECTED_ADV:
      PRINT( "Connected Advertising..\n" );
      break;      
    
    case GAPROLE_WAITING:
      if( pEvent->gap.opcode == GAP_END_DISCOVERABLE_DONE_EVENT )
      {
        PRINT( "Waiting for advertising..\n" );
      }
      else if( pEvent->gap.opcode == GAP_LINK_TERMINATED_EVENT )
      {
        Peripheral_LinkTerminated( pEvent );
        PRINT( "Disconnected.. Reason:%x\n",pEvent->linkTerminate.reason );
      }
      else if( pEvent->gap.opcode == GAP_LINK_ESTABLISHED_EVENT )
			{
				if( pEvent->gap.hdr.status != SUCCESS )
				{
					PRINT( "Waiting for advertising..\n" );
				}
				else
				{
					PRINT( "Error..\n" );
				}
			}
			else
			{
				PRINT( "Error..%x\n",pEvent->gap.opcode );
			}
      break;

    case GAPROLE_ERROR:
			PRINT( "Error..\n" );
      break;

    default:
      break;
  }
}

/*********************************************************************
 * @fn      performPeriodicTask
 *
 * @brief   Perform a periodic application task. This function gets
 *          called every five seconds as a result of the SBP_PERIODIC_EVT
 *          TMOS event. In this example, the value of the third
 *          characteristic in the SimpleGATTProfile service is retrieved
 *          from the profile, and then copied into the value of the
 *          the fourth characteristic.
 *
 * @param   none
 *
 * @return  none
 */
static void performPeriodicTask( void )
{
  uint8 notiData[SIMPLEPROFILE_CHAR4_LEN] = { 0x66,0x12 };
  peripheralChar4Notify( notiData, SIMPLEPROFILE_CHAR4_LEN );
}

/*********************************************************************
 * @fn      peripheralChar4Notify
 *
 * @brief   Prepare and send simpleProfileChar4 notification
 *
 * @param   pValue - data to notify
 *          len - length of data
 *
 * @return  none
 */
static void peripheralChar4Notify( uint8 *pValue, uint16 len )
{
  attHandleValueNoti_t noti;
  noti.len = len;
  noti.pValue = GATT_bm_alloc( peripheralConnList.connHandle, ATT_HANDLE_VALUE_NOTI, noti.len, NULL, 0 );
  tmos_memcpy( noti.pValue, pValue, noti.len );
  if( simpleProfile_Notify( peripheralConnList.connHandle, &noti ) != SUCCESS )
  {
    GATT_bm_free( (gattMsg_t *)&noti, ATT_HANDLE_VALUE_NOTI );
  }
	PRINT("Handle:%x",noti.handle);
}
  
/*********************************************************************
* @fn      simpleProfileChangeCB //接收到主机发送的数据  indication
 *
 * @brief   Callback from SimpleBLEProfile indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
/*  接收到主机发送的数据  indication */
static void simpleProfileChangeCB( uint8 paramID )
{

  switch( paramID )
  {
    case SIMPLEPROFILE_CHAR1:
		{
			uint8 newValue[SIMPLEPROFILE_CHAR1_LEN];

      SimpleProfile_GetParameter( SIMPLEPROFILE_CHAR1, newValue );
			newValue_len=sizeof(newValue);
			PRINT("newValue_len:%d\n",newValue_len);
			PRINT("newValue:%s\n",newValue);
			Wave_Config(newValue);
			PRINT("Fecy:%d\n",Fecy);
			PRINT("Wave_Choose:%x\n",Wave_Choose);
//			Wave_Hign=newValue[1];
//			Wave_Low=newValue[2];
//			Wave_Choose=0x0000;
//			Wave_Choose |= (Wave_Hign<<8);
//			Wave_Choose |= Wave_Low;
			flag=0;
			PRINT("profile ChangeCB CHAR1.. \n");
      break;
		}

    case SIMPLEPROFILE_CHAR3:
		{
			uint8 newValue[SIMPLEPROFILE_CHAR3_LEN];
      SimpleProfile_GetParameter( SIMPLEPROFILE_CHAR3, newValue );
			PRINT("profile ChangeCB CHAR3..\n");
      break;
		}

    default:
      // should not reach here!
      break;
  }
}

//以下代码为个人写的
/* 从机数据发送函数 */
static void SBP_SerialAppSendNoti(UINT8 *pBuffer,UINT16 length)
{
	UINT8 len;
	//判断数据包长度，最大为20字节
	if(length>20)
	{
		length=20;
	}
	else
	{
		len=length;
	}
	attHandleValueNoti_t pReport;
	pReport.len=len;
	pReport.pValue = GATT_bm_alloc( peripheralConnList.connHandle, ATT_HANDLE_VALUE_NOTI, pReport.len, NULL, 0 );
  tmos_memcpy( pReport.pValue, pBuffer, pReport.len );
  if( simpleProfile_Notify( peripheralConnList.connHandle, &pReport ) != SUCCESS )
  {
    GATT_bm_free( (gattMsg_t *)&pReport, ATT_HANDLE_VALUE_NOTI );
  }
	PRINT("Send_OK\n");
}

void value1_send_temture(void)
{
	value1[0]=Git_ADC_Tempture();
	SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR1, SIMPLEPROFILE_CHAR1_LEN, value1 );
}

void Wave_Config(PUINT8 b) 
{
	UINT8 Count_unit=0,count_number=0;
	UINT8 Unit_Fecy=0;
	
	for(Count_unit=0;Count_unit<newValue_len;Count_unit++)
	{
		if(b[Count_unit]=='M')
		{
			Unit_Fecy=Count_unit;
			for(count_number=0;count_number<Unit_Fecy;count_number++)
			{
				Fecy_number+=((b[count_number]-48)*pow(10,(Unit_Fecy-1-count_number)));
			}
			Fecy=Fecy_number*1000000;
			PRINT("Fecy_number:%d",Fecy_number);
		}
		else if(b[Count_unit] == 'K')
		{
			Unit_Fecy=Count_unit;
			printf("Unit_Fecy:%d",Unit_Fecy);
			for(count_number=0;count_number<Unit_Fecy;count_number++)
			{
				Fecy_number+=((b[count_number]-48)*pow(10,(Unit_Fecy-1-count_number)));
			}
			Fecy=Fecy_number*1000;
			PRINT("Fecy_number:%d",Fecy_number);
		}
		else if(b[Count_unit] == 'H')
		{
			Unit_Fecy=Count_unit;
			printf("Unit_Fecy:%d",Unit_Fecy);
			for(count_number=0;count_number<Unit_Fecy;count_number++)
			{
				Fecy_number+=((b[count_number]-48)*pow(10,(Unit_Fecy-1-count_number)));
			}
			Fecy=Fecy_number;
			PRINT("Fecy_number:%d",Fecy_number);
		}
	}
	if(b[Unit_Fecy+1] == '1')
	{
		Wave_Choose=0x2000;
	}
	else if(b[Unit_Fecy+1] == '2')
	{
		Wave_Choose=0x2002;
	}
	else if(b[Unit_Fecy+1] == '3')
	{
		Wave_Choose=0x2028;
	}
}

/*********************************************************************
*********************************************************************/
