#include "myspi.h"

void My_SPI_Init(void)
{
	GPIOA_SetBits(GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15);
	GPIOA_ModeCfg(GPIO_Pin_13, GPIO_ModeOut_PP_5mA);	
	GPIOA_ModeCfg(GPIO_Pin_14, GPIO_ModeOut_PP_5mA);	
	GPIOA_ModeCfg(GPIO_Pin_15, GPIO_ModeOut_PP_5mA);	
}


void Soft_SPI_Write(uint16_t a)
{
	uint16_t cnt;
//	CS=0;//拉低片选线
//	delay_us(10);
	for(cnt=0;cnt<16;cnt++)
	{
		Soft_SPI_SCLK_HIGH();//拉高CLK
		DelayUs(10);
		if(a &0x8000)
		{
			Soft_SPI_MOSI_HIGH();
		}
		else
		{
			Soft_SPI_MOSI_LOW();
		}
		a <<= 1;
		DelayUs(10);
		Soft_SPI_SCLK_LOW();//拉低CLK
		DelayUs(10);
	}
	Soft_SPI_SCLK_HIGH();
//	CS=1;
}

