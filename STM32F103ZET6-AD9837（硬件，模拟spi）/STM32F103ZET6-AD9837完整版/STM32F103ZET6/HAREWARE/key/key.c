#include "sys.h"
#include "key.h"
#include "delay.h"

void key_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOE,ENABLE);//使能PORTA,PORTE时钟
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入,按键没按下默认高电平
	GPIO_Init(GPIOE,&GPIO_InitStruct);//初始化GPIOE2，3，4 KEY0,1,2
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPD; //设置成上拉输入,按键没按下默认低电平
	GPIO_Init(GPIOA,&GPIO_InitStruct);//初始化GPIOA.0  ,KEY_UP
}

u8 key_scan(u8 mode)//mode 0 不连续按下，mode 1连续按下
{
	static u8 key_flag=1;//按键松开标志位
	if(mode==1)
		key_flag=1;
	if((key_flag==1)&&(KEY0==0||KEY1==0||KEY2==0||WK_UP==1))
	{
		delay_ms(20);
		key_flag=0;//按键按下，flag置0
		if(KEY0==0) return 1;
		else if(KEY1==0) return 2;
		else if(KEY2==0) return 3;
		else if(WK_UP==1) return 4;
	}
	else if(KEY0==1&&KEY1==1&&KEY2==1&&WK_UP==0) key_flag=1;//没有按键按下，松开，flag置1
	return 0;//无按键按下
}
