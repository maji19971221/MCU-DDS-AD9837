#include "74hc595.h"


void _74hc595_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);//使能GPIOA时钟
	
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;//初始化GPIOA5,6,7，速度50M，推挽式输出
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
}
void _74hc595_in(u8 a)
{
	u8 i;
	for(i=0;i<8;i++)
	{
		if(a&0x80)
			ser=1;
		else
			ser=0;
//		ser=a>>15;
		sck=0;
		__NOP();
		__NOP();
		sck=1;
		a<<=1;
	}
}

void _74hc595_out(void)
{
	rck=0;
	__NOP();
	__NOP();
	rck=1;
}
