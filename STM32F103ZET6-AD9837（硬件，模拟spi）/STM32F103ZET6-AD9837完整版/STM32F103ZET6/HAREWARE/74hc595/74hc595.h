#ifndef __74HC595_H_
#define __74HC595_H_

#include "sys.h"

#define ser PAout(5)
#define sck PAout(6)
#define rck PAout(7)

void _74hc595_in(u8);
void _74hc595_out(void);
void _74hc595_init(void);

#endif
