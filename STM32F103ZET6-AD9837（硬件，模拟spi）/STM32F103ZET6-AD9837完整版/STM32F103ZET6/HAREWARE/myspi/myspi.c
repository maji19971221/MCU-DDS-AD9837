#include "myspi.h"

void my_SPI_Init(void)
{
	/*定义一个GPIO_InitTypeDef类型的（基本IO）结构体*/
	GPIO_InitTypeDef GPIO_InitStructure;

	/***** 使能 GPIO 时钟 *****/

	/* 使能 FLASH_SPI引脚的GPIO时钟< SPI_CS; SPI_MOSI; SPI_MISO; SPI_SCK > （ F口 ）*/
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOF, ENABLE);
	/*  MOSI   F6,SCLK F8 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7|GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //PB6/7/8推挽输出 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOF, &GPIO_InitStructure);
	/*  CS F7 */
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //PB7推挽输出 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOF, &GPIO_InitStructure);

	/* < 配置 SPI_FLASH_SPI 引脚: MISO > */

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
	GPIO_Init(GPIOF, &GPIO_InitStructure);
	
	GPIO_SetBits(GPIOF, GPIO_Pin_8);//拉高时钟线，模拟模式3
	GPIO_SetBits(GPIOF, GPIO_Pin_7);//拉高片选线
}

void Soft_SPI_Write(uint16_t a)
{
	uint16_t cnt;
//	CS=0;//拉低片选线
//	delay_us(10);
	for(cnt=0;cnt<16;cnt++)
	{
		SCLK=1;//拉高CLK
		delay_us(10);
		if(a &0x8000)
		{
			MOSI=1;
		}
		else
		{
			MOSI=0;
		}
		a <<= 1;
		delay_us(10);
		SCLK=0;//拉低CLK
		delay_us(10);
	}
	SCLK=1;
//	CS=1;
}

uint32_t Soft_SPI_Read(void)
{
	uint32_t cnt;
	uint32_t Rxdata = 0;
	
	for(cnt=0;cnt<32;cnt++)
	{
		SCLK=1;
		delay_us(10);
		Rxdata <<= 1;
		if(GPIO_ReadInputDataBit(GPIOF, GPIO_Pin_9))
		{
			Rxdata |= 0x00000001;
		}
		SCLK=0;//拉高CLK
		delay_us(10);
	}
	return Rxdata;
}
//void Soft_SPI_Write(uint32_t a)
//{
//	uint32_t cnt;
//	CS=0;//拉低片选线
//	delay_us(10);
//	for(cnt=0;cnt<32;cnt++)
//	{
//		SCLK=1;//拉高CLK
//		delay_us(10);
//		if(a &0x80000000)
//		{
//			MOSI=1;
//		}
//		else
//		{
//			MOSI=0;
//		}
//		a <<= 1;
//		delay_us(10);
//		SCLK=0;//拉低CLK
//		delay_us(20);
//	}
//	SCLK=1;
//	CS=1;
//}

