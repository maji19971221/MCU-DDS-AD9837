#ifndef __MYSPI_H
#define __MYSPI_H
#include "sys.h"
#include "delay.h"	  

void my_SPI_Init(void);		 //��ʼ��SPI��
void Soft_SPI_Write(uint16_t a);
uint32_t Soft_SPI_Read(void);

#define	CS    PFout(7) 
#define	SCLK  PFout(8) 
#define	MOSI  PFout(6) 
#define MISO  PFin(9)
		 
#endif
