#include "ad9837.h"
#include "led.h"
#include "myspi.h"

void ad9837_Init(void)
{	
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );//PORTB时钟使能 

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;   // PB12 推挽 片选信号初始化
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
 	GPIO_SetBits(GPIOB,GPIO_Pin_12);
 
//  FSYNC=1;				//SPI ad9837不选中
	SPI2_Init();		   	//初始化SPI
	SPI2_SetSpeed(SPI_BaudRatePrescaler_32);//
} 

void WriteToAD9837_Freq(void)
{ 
	CS=0;//拉低片选线
	delay_us(10);
	Soft_SPI_Write(0X2100);//D13=1,D8=1,reset
	CS=1;
	
	delay_us(30);
	
	CS=0;//拉低片选线
	delay_us(10);
	Soft_SPI_Write(0x7264);//D13=1,D8=1,reset 300Hz  控制频率7264  4110
	CS=1;
	
	delay_us(30);
	
	CS=0;//拉低片选线
	delay_us(10); 
	Soft_SPI_Write(0x44aa);//D13=1,D8=1,reset  4fff  5481
	CS=1;
	
	delay_us(30);
		
	CS=0;//拉低片选线
	delay_us(10);
	Soft_SPI_Write(0XC000);//D13=1,D8=1,reset
	CS=1;

	
	delay_us(30);
	
	CS=0;//拉低片选线
	delay_us(10);
	Soft_SPI_Write(0x2000);//D13=1,D8=1,reset  控制波形   2000-sin  2002-三角   2028-方波
	CS=1;
	delay_us(30);
}




//void WriteToAD9837_Freq(void)
//{ 
//	FSYNC=0;
//	SPI2_ReadWriteByte(0X2100);//D13=1,D8=1,reset
//	delay_us(50);
//	FSYNC=1;
//	
//	delay_us(30);
//	
//	FSYNC=0;
//	SPI2_ReadWriteByte(0x7264);//D13=1,D8=1,reset 300Hz  控制频率7264  4110
//	delay_us(50);
//	FSYNC=1;
//	
//	delay_us(30);
//	
//	FSYNC=0;
//	SPI2_ReadWriteByte(0x4000);//D13=1,D8=1,reset  4fff  5481
//	delay_us(50);
//	FSYNC=1;
//	
//	delay_us(30);
//		
//	FSYNC=0;
//	SPI2_ReadWriteByte(0XC000);//D13=1,D8=1,reset
//	delay_us(50);
//	FSYNC=1;
//	
//	delay_us(30);
//	
//	FSYNC=0;
//	SPI2_ReadWriteByte(0x2028);//D13=1,D8=1,reset  控制波形   2000-sin  2002-三角   2028-方波
//	delay_us(50);
//	FSYNC=1;
//	delay_us(30);
//}


