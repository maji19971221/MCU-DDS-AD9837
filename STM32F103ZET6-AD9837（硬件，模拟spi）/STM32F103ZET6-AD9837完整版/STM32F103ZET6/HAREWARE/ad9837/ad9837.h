#ifndef __AD9837_H_
#define __AD9837_H_
#include "sys.h"
#include "spi.h"
#include "delay.h"

#define	FSYNC 		PBout(12)  		//W25QXX��Ƭѡ�ź�

void ad9837_Init(void);
void WriteToAD9837Word(u16 va);
void AD9837_Configration(void);
void WriteToAD9837_Freq(void);

#endif
