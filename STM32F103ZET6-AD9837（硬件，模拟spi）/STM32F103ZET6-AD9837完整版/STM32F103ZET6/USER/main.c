#include "stm32f10x.h"
#include "delay.h"
#include "LED.h"
#include "beep.h"
#include "key.h"
#include "74hc595.h"
#include "usart.h"
#include "exit.h"
#include "timer.h"
#include "spi.h"
#include "ad9837.h"
#include "myspi.h"

//u8 table[6]={0x01,0x02,0x04,0x08,0x10,0x20};
//u8 ascii_table_5x7[6]={0x3c,0x4a,0x49,0x49,0x30};
//u8 i=0;
u8 flag=0;

int main(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(115200);	 //波特率115200
	LED_Init();
	delay_init();
//	BEEP_Init();
//	key_Init();
//	EXTIX_Init();//内部包含key_init
//	TIM3_Int_Init(4999,7199); 
//	_74hc595_init();
//	ad9837_Init();
	my_SPI_Init();
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	RCC_MCOConfig(RCC_MCO_HSE);//36M???RCC_MCO_HSE   RCC_MCO_PLLCLK_Div2
	
	while(1)
	{
		if(flag==0)
		{
			delay_us(30);
			WriteToAD9837_Freq();
			flag=1;
		}
	}
}

